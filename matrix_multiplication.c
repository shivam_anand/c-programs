#include<stdio.h>
int main()
{
  int A[30][30], B[30][30], C[30][30], i, j, r1, r2, c1, c2, k;
  printf("Enter the order of Matrix A\n");
  scanf("%d%d",&r1,&c1);
  printf("Enter the order of Matrix B\n");
  scanf("%d%d",&r2,&c2);
  if (r1==c2) {
    /* code */
    printf("Enter the value of Matrix A\n");
    for(i=0;i<r1;i++){
      for(j=0;j<c1;j++){
        scanf("%d",&A[i][j]);
      }
    }
    printf("Enter the value of Matrix B\n");
    for(i=0;i<r2;i++){
      for(j=0;j<c2;j++){
        scanf("%d",&B[i][j]);
      }
    }
    for(i=0;i<r1;i++){
      for(j=0;j<c2;j++){
        for(k=0;k<c1;k++){
          C[i][j]=0;
        }
      }
    }
    for(i=0;i<r1;i++){
      for(j=0;j<c2;j++){
        for(k=0;k<c1;k++){
          C[i][j]=C[i][j]+A[i][k]*B[k][j];
        }
      }
    }
    printf("\nOutput:\n");
    for(i=0;i<r1;i++){
      for(j=0;j<c2;j++){
        printf("%d\t",C[i][j]);
      }
      printf("\n");
    }
  }
  else
  printf("Error\n");
  return 0;
}
