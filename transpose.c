#include<stdio.h>
int main()
{
  int A[3][3],i,j;
  printf("Enter the row and column of matrix\n");
  for(i=0;i<3;i++)
  {
    printf("ENter the element of row %d\n",i+1);
    for(j=0;j<3;j++)
    {
      scanf("%d",&A[i][j]);
    }
  }
  printf("Your Matrix is\n");
  for(i=0;i<3;i++)
  {
    for(j=0;j<3;j++)
    {
      printf("%d\t",A[i][j]);
    }
    printf("\n");
  }
  printf("Transposed Matrix :-\n");
  for(i=0;i<3;i++)
  {
    for(j=0;j<3;j++)
    {
      printf("%d\t",A[j][i]);
    }
    printf("\n");
  }
  return 0;
}
